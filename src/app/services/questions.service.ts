//define, what'll be used later on
import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Question, Quiz } from '../models/quiz.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QuestionsService {

  constructor(private http: HttpClient) {
  }

  public getQuizzes() : Observable<any> {
    return this.http.get(`./assets/quiz-list.json`);
    // return this.http.get(`./assets/quiz-list.json`).pipe(
    //   map((result: any[]) => {
    //     return result.map(r => new Quiz(r.label, r.name, r.description, r.fileName));
    //   })
    // );
  }

  public getQuestions(fileName: string): Observable<any> {
    return this.http.get(`./assets/${fileName}.json`);
    // return this.http.get(`./assets/${fileName}.json`).pipe(
    //   map((result: any[]) => {
    //     return result.map(r => new Question(r.label, r.choices));
    //   })
    // );
  }
}
