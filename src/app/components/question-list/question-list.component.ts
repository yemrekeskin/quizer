import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Answers, Choice, Question, Quiz } from 'src/app/models/quiz.model';
import { QuestionsService } from 'src/app/services/questions.service';

@Component({
  selector: 'app-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.scss']
})
export class QuestionListComponent implements OnInit {

  quiz: Quiz;
  answers: Answers;
  questions: Question[];
  currentQuestionIndex: number;
  showResults = false;

  constructor(
    private route: ActivatedRoute,
    private questionsService: QuestionsService) {

  }

  ngOnInit(): void {
    this.questionsService.getQuestions(this.route.snapshot.params.quizId)
      .subscribe(questions => {
        console.log(questions);

        this.questions = questions;
        this.answers = new Answers();
        this.currentQuestionIndex = 0;
      });
  }

  updateChoice(choice: Choice | any) {
    this.answers.values[this.currentQuestionIndex] = choice;
  }

  nextOrViewResults() {
    if (this.currentQuestionIndex === this.questions.length - 1) {
      this.showResults = true;
      return;
    }
    this.currentQuestionIndex++;
  }

  reset() {
    // this.quiz = undefined;
    // this.questions = undefined;
    // this.answers = undefined;
    // this.currentQuestionIndex = undefined;
  }

}
